FROM python:3.8-slim

RUN apt update && apt install -y gcc libpq-dev

WORKDIR /api

COPY requirements.txt .

RUN pip install -r requirements.txt

CMD gunicorn omni_latam.wsgi -b 0.0.0.0:8000