# Omni Latam Test

- [Docs](https://icy-pan-2f4.notion.site/OmniLatam-e29a864b91e441a98a25682c066edc48)
- The postman collection is in `omni.postman_collection.json` file.

## Start local project

1.  Create virtual environment `python3 -m venv env` and activate virtual environment:

    - Linux: `source env/bin/activate`
    - Windows: `source env/Scripts/activate`

2.  Run command `pip3 install -r requirements.txt`

3.  Go to the project folder with `cd <path_project>` and copy environment file `cp .env.example .env`

    - Config database:

      ```
      # Database configuration
      DB_HOST=
      DB_PORT=
      DB_NAME=
      DB_USER=
      DB_PASS=
      ```

4.  Run migrations `python3 manage.py migrate`

### Execute Unit Tests

1. Run command `python manage.py test <module>`

## Docker install

1. Install [Docker](https://docs.docker.com/engine/install)

2. Install [Docker Compose](https://docs.docker.com/compose/install/)

3. Start docker container `docker-compose up -d`

4. Create database `docker-compose exec db psql -U postgres -c 'create database omni'`

5. Run migrations `docker-compose exec api python manage.py migrate`

### Execute Unit Tests

1. Run command `docker-compose exec api python manage.py test <module>`
