import environ

__all__ = ('env')

env = environ.Env()

env.read_env(env.path('ENV_FILE_PATH', default=(
    environ.Path(__file__)-2).path('.env')())())
