from abc import ABC, abstractmethod, abstractproperty
from threading import Thread


class Message(ABC):

    def __init__(self, data):
        self.data = data

    @abstractmethod
    def _send(self):
        pass

    def send(self):
        task = Thread(target=self._send, args=[])
        task.setDaemon(True)
        task.start()
