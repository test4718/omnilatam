from django.contrib import admin
from django.urls import path, include

from products.views.product import ProductView
from payments.views.payment import PaymentView
from payments.views.payment_types import PaymentTypeView
from users.views.users import UserView, RegisterView
from orders.views.order import OrderView
from shipments.views.shipment import ShipmentView

urlpatterns = [
    path('products', ProductView.as_view(), name='products'),
    path('products/', include(('products.urls', 'products'), namespace='products')),

    path('payments_types', PaymentTypeView.as_view(), name='payments'),

    path('payments', PaymentView.as_view(), name='payments'),
    path('payments/', include(('payments.urls', 'payments'), namespace='payments')),

    path('users', UserView.as_view(), name='users'),
    path('users/', include(('users.urls', 'users'), namespace='users')),
    path('users/register', RegisterView.as_view(), name='users'),

    path('orders', OrderView.as_view(), name='orders'),
    path('orders/', include(('orders.urls', 'orders'), namespace='orders')),

    path('shipments', ShipmentView.as_view(), name='shipments'),
    path('shipments/', include(('shipments.urls', 'shipments'), namespace='shipments')),
]
