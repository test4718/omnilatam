from django.db import models
from users.models.user import User


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.Manager()

    class Meta:
        db_table = 'orders'
