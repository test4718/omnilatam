from django.db import models
from orders.models.order import Order
from payments.models.payment import Payment


class OrderPayment(models.Model):
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    payment = models.ForeignKey(Payment, on_delete=models.PROTECT)
    value = models.DecimalField(max_digits=18, decimal_places=10)

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.Manager()

    class Meta:
        db_table = 'order_payments'
