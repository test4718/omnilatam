from django.db import models
from orders.models.order import Order
from products.models.products import Product
from shipments.models.shipments import Shipment


class OrderProduct(models.Model):
    shipment = models.ForeignKey(Shipment, on_delete=models.PROTECT, null=True)
    order = models.ForeignKey(Order, on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.PROTECT)
    value = models.DecimalField(max_digits=18, decimal_places=10)
    date_time_delivery = models.DateTimeField(null=True)

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.Manager()

    class Meta:
        db_table = 'order_products'
