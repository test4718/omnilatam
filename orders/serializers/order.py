from rest_framework import serializers
from orders.models.order import Order
from django.db import transaction

from rest_framework.validators import UniqueValidator
from users.serializers.user import ListSerializer as UserListSerializer


class CreateSerializer(serializers.Serializer):
    @transaction.atomic
    def create(self, data):
        order = Order()
        order.user_id = self.context.id
        order.save()
        return order


class ListSerializer(serializers.ModelSerializer):
    user = UserListSerializer()

    class Meta:
        model = Order
        fields = ('id', 'user')
        depth = 1
