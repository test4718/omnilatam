from rest_framework import serializers
from django.db import transaction
from orders.models.order_payments import OrderPayment

from orders.validators.order import validate_order_exits
from payments.validators.payment import validate_payment_exits

from orders.serializers.order import ListSerializer as OrderListSerializer
from payments.serializers.payment import ListSerializer as PaymentListSerializer


class CreateSerializer(serializers.Serializer):
    order = serializers.IntegerField(required=True)
    payment = serializers.IntegerField(required=True)
    value = serializers.DecimalField(required=True,
                                     max_digits=18, decimal_places=10)

    def validate_order(self, value):
        validate_order_exits(value)
        return value

    def validate_payment(self, value):
        validate_payment_exits(value)
        return value

    @transaction.atomic
    def create(self, data):
        order_payment = OrderPayment()
        order_payment.order_id = data["order"]
        order_payment.payment_id = data["payment"]
        order_payment.value = data["value"]
        order_payment.save()
        return order_payment


class UpdateSerializer(serializers.Serializer):
    value = serializers.DecimalField(required=False,
                                     max_digits=18, decimal_places=10)

    @transaction.atomic
    def update(self, order_payment, data):
        if "value" in data:
            order_payment.value = data.get('value', order_payment.value)

        order_payment.save()
        return order_payment


class ListSerializer(serializers.ModelSerializer):
    order = OrderListSerializer()
    payment = PaymentListSerializer()

    class Meta:
        model = OrderPayment
        fields = (
            "id",
            "order",
            "payment",
            "value"
        )
        depth = 1
