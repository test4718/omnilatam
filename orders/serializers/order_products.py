from rest_framework import serializers
from orders.models.order_products import OrderProduct
from django.db import transaction

from products.models.products import Product

from orders.validators.order import validate_order_exits
from products.validators.product import validate_product_exits
from shipments.validators.shipment import validate_shipment_exits

from orders.serializers.order import ListSerializer as OrderListSerializer
from shipments.serializers.shipment import ListSerializer as ShipmentListSerializer
from products.serializers.product import ListSerializer as ProductListSerializer

from omni_latam.notifications.types.email import Email
from omni_latam.notifications.types.sns import Sns


class CreateSerializer(serializers.Serializer):
    order = serializers.IntegerField()
    product = serializers.IntegerField()

    def validate_order(self, value):
        validate_order_exits(value)
        return value

    def validate_product(self, value):
        validate_product_exits(value)
        return value

    @transaction.atomic
    def create(self, data):
        order_product = OrderProduct()
        order_product.order_id = data["order"]
        order_product.product_id = data["product"]
        product = Product.objects.get(id=data["product"])
        order_product.value = product.value
        order_product.save()
        return order_product


class UpdateSerializer(serializers.Serializer):
    active = serializers.BooleanField(required=False)
    value = serializers.DecimalField(required=False,
                                     max_digits=18, decimal_places=10)
    shipment = serializers.IntegerField(required=False)
    date_time_delivery = serializers.DateTimeField(required=False)

    def validate_shipment(self, value):
        validate_shipment_exits(value)
        return value

    @transaction.atomic
    def update(self, order_product, data):
        if "active" in data:
            order_product.active = data.get('active', order_product.active)
        if "value" in data:
            order_product.value = data.get('value', order_product.value)
        if "shipment" in data:
            order_product.shipment_id = data.get(
                'shipment', order_product.shipment_id)
        if "date_time_delivery" in data:
            order_product.date_time_delivery = data.get(
                'date_time_delivery', order_product.date_time_delivery)

        order_product.save()
        self.notify(data, order_product)
        return order_product

    def notify(self, data, order_product):
        if "date_time_delivery" in data and "shipment" in data:
            Sns(order_product).send()
            Email(order_product).send()


class ListSerializer(serializers.ModelSerializer):
    order = OrderListSerializer()
    shipment = ShipmentListSerializer()
    product = ProductListSerializer()

    class Meta:
        model = OrderProduct
        fields = (
            "id",
            "order",
            "shipment",
            "product",
            "date_time_delivery",
            "value"
        )
        depth = 1
