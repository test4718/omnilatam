from rest_framework import status
from rest_framework.test import APITestCase
from users.models.user import User


class OrdersTest(APITestCase):

    token = ""

    def _login(self):
        data = {
            "first_name": "prueba",
            "last_name": "prueba",
            "email": "pr@pr.com",
            "password": "clave1234"
        }
        response = self.client.post('/users/register', data, format='json')
        self.token = f'Token {response.data["token"]}'

    def _create_products(self):
        response = self.client.post(
            '/products', {
                "name": "prueba2",
                "description": "prueba2",
                "value": 3000.05
            }, format='json', HTTP_AUTHORIZATION=self.token)

        return [response.data["id"]]

    def test_create(self):
        self._login()

        response = self.client.post(
            '/orders', {"products": self._create_products()}, format='json', HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


class OrderProductTest(APITestCase):

    def _login(self):
        data = {
            "first_name": "prueba",
            "last_name": "prueba",
            "email": "pr@pr.com",
            "password": "clave1234"
        }
        response = self.client.post('/users/register', data, format='json')

        self.token = f'Token {response.data["token"]}'

    def _create_products(self, data):
        response = self.client.post(
            '/products', data, format='json', HTTP_AUTHORIZATION=self.token)
        return response.data["id"]

    def _create_order(self):

        return self.client.post(
            '/orders', {"products": [self._create_products({
                "name": "prueba2",
                "description": "prueba2",
                "value": 3000.05
            })]}, format='json', HTTP_AUTHORIZATION=self.token)

    def _product_create(self, data):
        order = self._create_order()
        product = self._create_products(data)
        return self.client.post(
            f'/orders/{order.data["id"]}/products', {"product": product}, format='json', HTTP_AUTHORIZATION=self.token)

    def test_create(self):
        self._login()
        response = self._create_order()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_product_create(self):
        self._login()
        response = self._product_create({
            "name": "prueba3",
            "description": "prueba3",
            "value": 3000.05
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_product_update(self):
        self._login()
        response = self._product_create({
            "name": "prueba6",
            "description": "prueba6",
            "value": 3000.05
        })
        response = self.client.patch(
            f'/orders/products/{response.data["id"]}', {
                "active": False,
                "value": 4000.06
            }, format='json', HTTP_AUTHORIZATION=self.token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
