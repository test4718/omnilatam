from django.urls import path

from orders.views.order_product import OrderProductView
from orders.views.order_payment import OrderPaymentView

urlpatterns = [
    path(
        route='<int:order_id>/products',
        view=OrderProductView.as_view(),
        name='order_product'
    ),
    path(
        route='<int:order_id>/payments',
        view=OrderPaymentView.as_view(),
        name='order_product'
    ),
    path(
        route='products/<int:order_product_id>',
        view=OrderProductView.as_view(),
        name='order_product'
    ),
    path(
        route='payments',
        view=OrderPaymentView.as_view(),
        name='order_payment'
    ),
    path(
        route='payments/<int:order_payment_id>',
        view=OrderPaymentView.as_view(),
        name='order_payment'
    )
]
