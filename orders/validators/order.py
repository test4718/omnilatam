from orders.models.order import Order
from omni_latam.exceptions import ValidationError


def validate_order_exits(id):
    try:
        Order.objects.get(id=id)
    except Order.DoesNotExist:
        raise ValidationError("The Order doesn't exists")
