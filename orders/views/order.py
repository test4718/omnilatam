from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from orders.serializers.order import ListSerializer, CreateSerializer
from orders.models.order import Order


class OrderView(GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        response = ListSerializer(self.paginate_queryset(Order.objects.all()), many=True).data
        return self.get_paginated_response(response)

    def post(self, request):
        serializer = CreateSerializer(data=request.data, context=request.user)
        serializer.is_valid(raise_exception=True)
        order = serializer.save()
        return Response({"id": order.id}, status=status.HTTP_201_CREATED)
