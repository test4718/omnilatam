from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from orders.serializers.order_payment import CreateSerializer, UpdateSerializer, ListSerializer
from orders.models.order_payments import OrderPayment


class OrderPaymentView(GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, order_id):
        response = ListSerializer(self.paginate_queryset(OrderPayment.objects.all()), many=True).data
        return self.get_paginated_response(response)

    def post(self, request):
        serializer = CreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        order_payment = serializer.save()
        return Response({"id": order_payment.id}, status=status.HTTP_201_CREATED)

    def patch(self, request, order_payment_id):
        serializer = UpdateSerializer(OrderPayment.objects.get(id=order_payment_id),
                                      data=request.data)
        serializer.is_valid(raise_exception=True)
        order_payment = serializer.save()
        return Response({"id": order_payment.id}, status=status.HTTP_200_OK)
