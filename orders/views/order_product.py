from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from orders.serializers.order_products import CreateSerializer, UpdateSerializer,ListSerializer
from orders.models.order_products import OrderProduct


class OrderProductView(GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, order_id):
        response = ListSerializer(self.paginate_queryset(OrderProduct.objects.all()), many=True).data
        return self.get_paginated_response(response)

    def post(self, request, order_id):
        request.data["order"] = order_id
        serializer = CreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        order_product = serializer.save()
        return Response({"id": order_product.id}, status=status.HTTP_201_CREATED)

    def patch(self, request, order_product_id):
        serializer = UpdateSerializer(OrderProduct.objects.get(id=order_product_id),
                                      data=request.data)
        serializer.is_valid(raise_exception=True)
        order_product = serializer.save()
        return Response({"id": order_product.id}, status=status.HTTP_200_OK)
