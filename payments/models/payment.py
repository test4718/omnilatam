from django.db import models
from .payment_type import PaymentType

class Payment(models.Model):
    value = models.DecimalField(max_digits=18, decimal_places=10)
    payment_type = models.ForeignKey(PaymentType, on_delete=models.PROTECT)

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.Manager()

    class Meta:
        db_table = 'payments'
