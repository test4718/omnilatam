from django.db import models


class PaymentType(models.Model):
    name = models.TextField(max_length=128, unique=True)

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.Manager()

    class Meta:
        db_table = 'payment_types'
