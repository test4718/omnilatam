from rest_framework import serializers
from payments.models.payment import Payment
from django.db import transaction

from payments.validators.payment_type import validate_payment_type_exits
from payments.serializers.payment_type import ListSerializer as PaymentTypeListSerializer


class CreateSerializer(serializers.Serializer):
    payment_type = serializers.IntegerField()
    value = serializers.DecimalField(
        max_digits=18, decimal_places=10, required=True)

    def validate_payment_type(self, value):
        validate_payment_type_exits(value)
        return value

    @transaction.atomic
    def create(self, data):
        payment = Payment()
        payment.payment_type_id = data["payment_type"]
        payment.value = data["value"]
        payment.save()
        return payment


class UpdateSerializer(serializers.Serializer):
    payment_type = serializers.IntegerField(required=False)
    value = serializers.DecimalField(
        max_digits=18, decimal_places=10, required=False)

    def validate_payment_type(self, value):
        validate_payment_type_exits(value)
        return value

    @transaction.atomic
    def update(self, payment, data):
        if "value" in data:
            payment.value = data.get('value', payment.value)

        if "payment_type" in data:
            payment.payment_type_id = data.get(
                'payment_type', payment.payment_type_id)
        payment.save()
        return payment


class ListSerializer(serializers.ModelSerializer):

    payment_type = PaymentTypeListSerializer()

    class Meta:
        model = Payment
        fields = ('id',
                  "value",
                  "payment_type")
        depth = 1
