from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from django.db import transaction

from payments.models.payment_type import PaymentType


class CreateSerializer(serializers.Serializer):
    name = serializers.CharField(required=True, min_length=3, max_length=128, validators=[
        UniqueValidator(queryset=PaymentType.objects.all())])

    @transaction.atomic
    def create(self, data):
        payment_type = PaymentType()
        payment_type.name = data["name"]
        payment_type.save()
        return payment_type


class ListSerializer(serializers.ModelSerializer):

    class Meta:
        model = PaymentType
        fields = ("id","name")
        depth = 1
