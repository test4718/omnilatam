from rest_framework import status
from rest_framework.test import APITestCase
from users.models.user import User


class PaymentTest(APITestCase):

    token = ""

    def _login(self):
        data = {
            "first_name": "prueba",
            "last_name": "prueba",
            "email": "pr@pr.com",
            "password": "clave1234"
        }
        response = self.client.post('/users/register', data, format='json')
        self.token = f'Token {response.data["token"]}'

    def _create(self, data):
        return self.client.post('/payments', data, format='json', HTTP_AUTHORIZATION=self.token)

    def _update(self, id, data):
        return self.client.patch(f'/payments/{id}', data, format='json', HTTP_AUTHORIZATION=self.token)

    def _create_payment_type(self, data):
        return self.client.post('/payments_types', data, format='json', HTTP_AUTHORIZATION=self.token)

    def test_create(self):
        self._login()
        response = self._create_payment_type({"name": "tarjeta"})
        response = self._create({
            "payment_type": response.data["id"],
            "value": 23000.09
        })
        
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        self._login()
        response = self._create_payment_type({"name": "tarjeta"})
        response = self._create({
            "payment_type": response.data["id"],
            "value": 23000.09
        })
        response = self._update(response.data["id"], {
            "value": 24000.09
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
