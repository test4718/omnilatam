from django.urls import path

from payments.views.payment import PaymentView

urlpatterns = [
    path(
        route='<int:payment_id>',
        view=PaymentView.as_view(),
        name='payment'
    )
]
