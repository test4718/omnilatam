from payments.models.payment import Payment
from omni_latam.exceptions import ValidationError


def validate_payment_exits(id):
    try:
        Payment.objects.get(id=id)
    except Payment.DoesNotExist:
        raise ValidationError("The Payment doesn't exists")
