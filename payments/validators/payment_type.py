from payments.models.payment_type import PaymentType
from omni_latam.exceptions import ValidationError


def validate_payment_type_exits(id):
    try:
        PaymentType.objects.get(id=id)
    except PaymentType.DoesNotExist:
        raise ValidationError("The PaymentType doesn't exists")
