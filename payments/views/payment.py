from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from payments.serializers.payment import ListSerializer, CreateSerializer, UpdateSerializer
from payments.models.payment import Payment


class PaymentView(GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        response = ListSerializer(self.paginate_queryset(Payment.objects.all()), many=True).data
        return self.get_paginated_response(response)

    def post(self, request):
        serializer = CreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        payment = serializer.save()
        return Response(ListSerializer(payment, many=False).data, status=status.HTTP_201_CREATED)

    def patch(self, request, payment_id):
        serializer = UpdateSerializer(Payment.objects.get(id=payment_id),
                                      data=request.data)
        serializer.is_valid(raise_exception=True)
        payment = serializer.save()
        return Response(ListSerializer(payment, many=False).data, status=status.HTTP_200_OK)
