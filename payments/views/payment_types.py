from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from payments.serializers.payment_type import ListSerializer, CreateSerializer
from payments.models.payment_type import PaymentType


class PaymentTypeView(GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        response = ListSerializer(self.paginate_queryset(PaymentType.objects.all()), many=True).data
        return self.get_paginated_response(response)

    def post(self, request):
        serializer = CreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        payment_type = serializer.save()
        return Response(ListSerializer(payment_type, many=False).data, status=status.HTTP_201_CREATED)
