from django.db import models


class Product(models.Model):
    name = models.TextField(max_length=128, unique=True)
    description = models.TextField(max_length=256)
    active = models.BooleanField(default=True)
    value = models.DecimalField(max_digits=18, decimal_places=10)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.Manager()

    class Meta:
        db_table = 'products'
