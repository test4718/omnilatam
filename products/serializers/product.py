from rest_framework import serializers
from products.models.products import Product
from django.db import transaction
from rest_framework.validators import UniqueValidator


class CreateSerializer(serializers.Serializer):
    name = serializers.CharField(
        required=True, min_length=3, max_length=128, allow_null=False, validators=[
            UniqueValidator(queryset=Product.objects.all())])
    description = serializers.CharField(
        required=True, min_length=3, max_length=256, allow_null=False)
    value = serializers.DecimalField(
        max_digits=18, decimal_places=10, required=True)

    @transaction.atomic
    def create(self, data):
        product = Product()
        product.name = data["name"]
        product.description = data["description"]
        product.value = data["value"]
        product.save()
        return product


class UpdateSerializer(serializers.Serializer):
    name = serializers.CharField(min_length=3, max_length=128, validators=[
        UniqueValidator(queryset=Product.objects.all())])
    description = serializers.CharField(min_length=3, max_length=256)
    value = serializers.DecimalField(max_digits=18, decimal_places=10)

    @transaction.atomic
    def update(self, product, data):
        if "name" in data:
            product.name = data.get('name', product.name)
        if "description" in data:
            product.description = data.get('description', product.description)
        if "value" in data:
            product.value = data.get('value', product.value)
        product.save()
        return product


class ListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ('id','name', 'description', 'value')
        depth = 1
