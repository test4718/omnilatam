from rest_framework import status
from rest_framework.test import APITestCase
from users.models.user import User


class ProductTest(APITestCase):

    def _login(self):
        data = {
            "first_name": "prueba",
            "last_name": "prueba",
            "email": "pr@pr.com",
            "password": "clave1234"
        }
        response = self.client.post('/users/register', data, format='json')
        return f'Token {response.data["token"]}'

    def test_create_and_update(self):

        token = self._login()

        response = self.client.post(
            '/products', {
                "name": "prueba2",
                "description": "prueba2",
                "value": 3000.05
            }, format='json', HTTP_AUTHORIZATION=token)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = self.client.patch(
            f'/products/{response.data["id"]}', {
                "name": "prueba1",
                "description": "prueba1",
                "value": 5000.05
            }, format='json', HTTP_AUTHORIZATION=token)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["name"], "prueba1")
        self.assertEqual(response.data["description"], "prueba1")
        self.assertEqual(float(response.data["value"]), 5000.05)
