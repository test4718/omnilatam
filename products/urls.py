from django.urls import path

from products.views.product import ProductView

urlpatterns = [
    path(
        route='<int:product_id>',
        view=ProductView.as_view(),
        name='update_product'
    )
]