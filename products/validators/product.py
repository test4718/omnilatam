from products.models.products import Product
from omni_latam.exceptions import ValidationError


def validate_product_exits(id):
    try:
        Product.objects.get(id=id)
    except Product.DoesNotExist:
        raise ValidationError("The Product doesn't exists")
