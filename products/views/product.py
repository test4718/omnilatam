from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from products.serializers.product import ListSerializer, CreateSerializer, UpdateSerializer
from products.models.products import Product


class ProductView(GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        response = ListSerializer(self.paginate_queryset(Product.objects.all()), many=True).data
        return self.get_paginated_response(response)

    def post(self, request):
        serializer = CreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        product = serializer.save()
        return Response(ListSerializer(product, many=False).data, status=status.HTTP_201_CREATED)

    def patch(self, request, product_id):
        serializer = UpdateSerializer(Product.objects.get(id=product_id),
                                      data=request.data)
        serializer.is_valid(raise_exception=True)
        product = serializer.save()
        return Response(ListSerializer(product, many=False).data, status=status.HTTP_200_OK)
