from django.db import models


class Shipment(models.Model):
    address = models.TextField(max_length=256)
    description = models.TextField(max_length=256)
    longitude = models.DecimalField(max_digits=18, decimal_places=10)
    latitude = models.DecimalField(max_digits=18, decimal_places=10)

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.Manager()

    class Meta:
        db_table = 'shipments'
