from rest_framework import serializers
from shipments.models.shipments import Shipment
from django.db import transaction


class CreateSerializer(serializers.Serializer):
    address = serializers.CharField(
        required=True, min_length=3, max_length=128, allow_null=False)
    description = serializers.CharField(
        required=True, min_length=3, max_length=256, allow_null=False)
    longitude = serializers.DecimalField(
        max_digits=18, decimal_places=10, required=True)
    latitude = serializers.DecimalField(
        max_digits=18, decimal_places=10, required=True)

    @transaction.atomic
    def create(self, data):
        shipment = Shipment()
        shipment.address = data["address"]
        shipment.description = data["description"]
        shipment.longitude = data["longitude"]
        shipment.latitude = data["latitude"]
        shipment.save()
        return shipment


class UpdateSerializer(serializers.Serializer):
    address = serializers.CharField(
        required=False, min_length=3, max_length=128, allow_null=False)
    description = serializers.CharField(
        required=False, min_length=3, max_length=256, allow_null=False)
    longitude = serializers.DecimalField(
        max_digits=18, decimal_places=10, required=False)
    latitude = serializers.DecimalField(
        max_digits=18, decimal_places=10, required=False)

    @transaction.atomic
    def update(self, shipment, data):

        if "address" in data:
            shipment.address = data.get(
                'address', shipment.address)

        if "description" in data:
            shipment.description = data.get(
                'description', shipment.description)

        if "longitude" in data:
            shipment.longitude = data.get(
                'longitude', shipment.longitude)

        if "latitude" in data:
            shipment.latitude = data.get(
                'latitude', shipment.latitude)

        shipment.save()
        return shipment


class ListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Shipment
        fields = (
            "id",
            "address",
            "description",
            "longitude",
            "latitude",
        )
        depth = 1
