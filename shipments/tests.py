from rest_framework import status
from rest_framework.test import APITestCase


class ShipmentTest(APITestCase):

    token = ""

    def _login(self):
        data = {
            "first_name": "prueba",
            "last_name": "prueba",
            "email": "pr@pr.com",
            "password": "clave1234"
        }
        response = self.client.post('/users/register', data, format='json')
        self.token = f'Token {response.data["token"]}'

    def _create(self, data):
        response = self.client.post(
            '/shipments', data, format='json', HTTP_AUTHORIZATION=self.token)
        return response

    def _update(self, id, data):
        response = self.client.patch(
            f'/shipments/{id}', data, format='json', HTTP_AUTHORIZATION=self.token)
        return response

    def test_create(self):
        self._login()
        response = self._create({
            "address": "calle falsa",
            "description": "envio a calle falsa",
            "longitude": 4.6518084,
            "latitude": -74.1283153
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):
        self._login()
        response = self._create({
            "address": "calle falsa",
            "description": "envio a calle falsa",
            "longitude": 4.6518084,
            "latitude": -74.1283153
        })

        response = self._update(response.data["id"], {
            "address": "calle real",
            "description": "envio a calle real",
            "longitude": 4.6518094,
            "latitude": -74.1283163
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["address"], "calle real")
        self.assertEqual(response.data["description"], "envio a calle real")
        self.assertEqual(float(response.data["longitude"]), 4.6518094)
        self.assertEqual(float(response.data["latitude"]), -74.1283163)
