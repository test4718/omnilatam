from django.urls import path

from shipments.views.shipment import ShipmentView

urlpatterns = [
    path(
        route='<int:shipment_id>',
        view=ShipmentView.as_view(),
        name='shipment'
    )
]
