from shipments.models.shipments import Shipment
from omni_latam.exceptions import ValidationError


def validate_shipment_exits(id):
    try:
        Shipment.objects.get(id=id)
    except Shipment.DoesNotExist:
        raise ValidationError("The Shipment doesn't exists")
