from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework import status

from shipments.serializers.shipment import ListSerializer, CreateSerializer, UpdateSerializer
from shipments.models.shipments import Shipment


class ShipmentView(GenericAPIView):

    def get(self, request):
        response = ListSerializer(self.paginate_queryset(Shipment.objects.all()), many=True).data
        return self.get_paginated_response(response)

    def post(self, request):
        serializer = CreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        shipment = serializer.save(raise_exception=True)
        return Response(ListSerializer(shipment, many=False).data, status=status.HTTP_201_CREATED)

    def patch(self, request, shipment_id):
        serializer = UpdateSerializer(Shipment.objects.get(id=shipment_id),
                                      data=request.data)
        serializer.is_valid(raise_exception=True)
        shipment = serializer.save(raise_exception=True)
        return Response(ListSerializer(shipment, many=False).data, status=status.HTTP_200_OK)
