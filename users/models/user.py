from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    first_name = models.TextField(max_length=128)
    last_name = models.TextField(max_length=128)
    email = models.TextField(max_length=68, unique=True)
    password = models.TextField(max_length=256)
    token = models.TextField(max_length=256)

    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = models.Manager()

    class Meta:
        db_table = 'users'
