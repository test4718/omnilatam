from rest_framework import serializers
from rest_framework.authtoken.models import Token
from django.contrib.auth.hashers import check_password

from omni_latam.exceptions import ValidationError
from users.models.user import User


class LogSerializer(serializers.Serializer):
    email = serializers.EmailField(
        required=True, min_length=3, max_length=256, allow_null=False)
    password = serializers.CharField(
        required=True, min_length=3, max_length=256, allow_null=False)

    def validate(self, attrs):
        try:
            user = User.objects.get(email=attrs["email"])

            if(not check_password(attrs["password"], user.password)):
                raise ValidationError('Invalid credentials')

            self.context['user'] = user

        except User.DoesNotExist:
            raise ValidationError('Invalid credentials')
        except:
            raise ValidationError('Invalid credentials')

        return attrs

    def login(self):
        token = Token.objects.get_or_create(user=self.context['user'])[0].key
        return {"token": token}
