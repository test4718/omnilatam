from rest_framework import serializers
from users.models.user import User
from django.db import transaction
from django.core.validators import validate_email
from rest_framework.validators import UniqueValidator
from rest_framework.authtoken.models import Token


class CreateSerializer(serializers.Serializer):
    first_name = serializers.CharField(
        required=True, min_length=3, max_length=128, allow_null=False)
    last_name = serializers.CharField(
        required=True, min_length=3, max_length=128, allow_null=False)
    email = serializers.EmailField(
        validators=[validate_email, UniqueValidator(queryset=User.objects.all())])
    password = serializers.CharField(
        required=True, min_length=3, max_length=256, allow_null=False)

    @transaction.atomic
    def create(self, data):
        user = User()
        user.first_name = data["first_name"]
        user.last_name = data["last_name"]
        user.email = data["email"]
        user.username = data["email"]
        user.set_password(data["password"])
        user.save()
        token = Token.objects.get_or_create(user=user)[0].key
        return {"token": token}


class UpdateSerializer(serializers.Serializer):
    first_name = serializers.CharField(
        required=False, min_length=3, max_length=128, allow_null=False)
    last_name = serializers.CharField(
        required=False, min_length=3, max_length=128, allow_null=False)
    password = serializers.CharField(
        required=False, min_length=3, max_length=256, allow_null=False)

    @transaction.atomic
    def update(self, user, data):
        if "first_name" in data:
            user.first_name = data.get('first_name', user.first_name)

        if "last_name" in data:
            user.last_name = data.get('last_name', user.last_name)

        if "password" in data:
            user.set_password(data["password"])

        user.save()
        return user


class ListSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email')
        depth = 1
