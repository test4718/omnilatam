from rest_framework import status
from rest_framework.test import APITestCase
from users.models.user import User


class UserTest(APITestCase):

    def _create(self, data):
        return self.client.post('/users/register', data, format='json')

    def _update(self, token, data):
        return self.client.patch(
            '/users', data, format='json', HTTP_AUTHORIZATION=token)

    def test_create(self):
        response = self._create({
            "first_name": "prueba",
            "last_name": "prueba",
            "email": "pr@pr.com",
            "password": "clave1234"
        })
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_update(self):

        response = self._create({
            "first_name": "prueba",
            "last_name": "prueba",
            "email": "pr@pr.com",
            "password": "clave1234"
        })

        response = self._update(f'Token {response.data["token"]}', {
            "first_name": "prueba2",
            "last_name": "prueba2",
            "password": "clave1234"
        })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["first_name"], "prueba2")
        self.assertEqual(response.data["last_name"], "prueba2")

    def test_login(self):
        self._create({
            "first_name": "prueba",
            "last_name": "prueba",
            "email": "pr@pr.com",
            "password": "clave1234"
        })

        response = self.client.post('/users/login', {
            "email": "pr@pr.com",
            "password": "clave1234"
        }, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
