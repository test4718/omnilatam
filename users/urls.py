from django.urls import path

from users.views.users import UserView, LoginView, LogoutView

urlpatterns = [
    path(
        route='login',
        view=LoginView.as_view(),
        name='users'
    ),
    path(
        route='logout',
        view=LogoutView.as_view(),
        name='users'
    )
]
