from rest_framework.response import Response
from rest_framework.generics import GenericAPIView
from rest_framework import status
from rest_framework.permissions import IsAuthenticated

from users.serializers.user import ListSerializer, CreateSerializer, UpdateSerializer
from users.serializers.login import LogSerializer
from users.models.user import User


class RegisterView(GenericAPIView):
    
    def post(self, request):
        serializer = CreateSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response(user, status=status.HTTP_201_CREATED)

class UserView(GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request):
        response = ListSerializer(self.paginate_queryset(User.objects.all()), many=True).data       
        return self.get_paginated_response(response)

    def patch(self, request):
        serializer = UpdateSerializer(request.user,
                                      data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response(ListSerializer(user, many=False).data, status=status.HTTP_200_OK)


class LoginView(GenericAPIView):

    def post(self, request):
        serializer = LogSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        return Response(serializer.login(), status=status.HTTP_200_OK)


class LogoutView(GenericAPIView):

    permission_classes = [IsAuthenticated]

    def get(self, request):
        request.user.auth_token.delete()
        return Response({}, status=status.HTTP_200_OK)
